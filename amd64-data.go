// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
import "strconv"
import "strings"

func (arch *AMD64_Arch) GenerateExtern(ext *ExternDef) {
	out_name := arch.EncodeName(ext.name)

	OutLine("extern %s", out_name)
	OutLine("")
}

func (arch *AMD64_Arch) GenerateVar(va *VarDef) {
	out_name := arch.EncodeName(va.name)

	if va.public {
		OutLine("global %s", out_name)
	}

	// if all zeroes, place in BSS section
	if va.data.Len() == 1 && ! va.rom {
		child := va.data.children[0]
		if child.kind == ND_Zeroes {
			arch.GenerateZeroVar(out_name, child.offset)
			return
		}
	}

	if va.rom {
		arch.Section("text")
	} else {
		arch.Section("data")
	}

	OutLine("%s:", out_name)

	for _, t := range va.data.children {
		arch.Data_Node(t)
	}

	OutIns("align", "16")
	OutLine("")
}

func (arch *AMD64_Arch) GenerateZeroVar(out_name string, size int64) {
	arch.Section("bss")

	OutLine("%s:", out_name)
	OutIns("resb", "%d", size)
	OutIns("alignb", "16")
	OutLine("")
}

//----------------------------------------------------------------------

func (arch *AMD64_Arch) Data_Node(data *Node) {
	if data.IsLiteral() {
		arch.Data_Literal(data)
		return
	}

	switch data.kind {
	case ND_Zeroes:
		arch.Data_Zeroes(data.offset)

	case NP_GlobPtr:
		arch.Data_Literal(data)

	default:
		panic("weird data")
	}
}

func (arch *AMD64_Arch) Data_Zeroes(count int64) {
	if count > 0 {
		num_str := strconv.FormatInt(count, 10)
		OutIns("times " + num_str, "db 0")
	}
}

func (arch *AMD64_Arch) Data_Literal(t *Node) {
	keyword := "dq"
	switch t.ty.bits {
	case  8: keyword = "db"
	case 16: keyword = "dw"
	case 32: keyword = "dd"
	}

	str_val := arch.DataValue(t)

	OutIns(keyword, "%s", str_val)
}

//----------------------------------------------------------------------

func (arch *AMD64_Arch) DataValue(lit *Node) string {
	switch lit.kind {
	case NL_Integer, NL_Float, NL_Char:
		return lit.str

	case NL_FltSpec:
		switch lit.str {
		case "+INF": return "__Infinity__"
		case "-INF": return "-__Infinity__"
		case "QNAN": return "__QNaN__"
		case "SNAN": return "__SNaN__"
		default:
			panic("unknown NL_FltSpec literal")
		}

	case NL_String:
		panic("NL_String to DataValue")

	case NP_GlobPtr:
		return arch.EncodeDefName(lit.def)

	default:
		panic("odd node to DataValue: " + lit.String())
	}
}

//----------------------------------------------------------------------

func (arch *AMD64_Arch) WriteFloats() {
	if len(arch.f32_consts) > 0 {
		values := make([]string, len(arch.f32_consts))
		for v, idx := range arch.f32_consts {
			values[idx] = v
		}

		OutLine("__float32_values:")

		for _, v := range values {
			OutIns("dd", "%s", v)
		}
		OutLine("")
	}

	if len(arch.f64_consts) > 0 {
		values := make([]string, len(arch.f64_consts))
		for v, idx := range arch.f64_consts {
			values[idx] = v
		}

		OutLine("__float64_values:")

		for _, v := range values {
			OutIns("dq", "%s", v)
		}
		OutLine("")
	}
}

func (arch *AMD64_Arch) AddFloat32(s string) string {
	idx, exist := arch.f32_consts[s]
	if !exist {
		idx = len(arch.f32_consts)
		arch.f32_consts[s] = idx
	}
	return "[rel __float32_values+" + strconv.Itoa(4 * idx) + "]"
}

func (arch *AMD64_Arch) AddFloat64(s string) string {
	idx, exist := arch.f64_consts[s]
	if !exist {
		idx = len(arch.f64_consts)
		arch.f64_consts[s] = idx
	}
	return "[rel __float64_values+" + strconv.Itoa(8 * idx) + "]"
}

//----------------------------------------------------------------------

func (arch *AMD64_Arch) EncodeDefName(def *Definition) string {
	switch def.kind {
	case DEF_Extern:
		ext := def.d_extern
		return arch.EncodeName(ext.name)

	case DEF_Func:
		fu := def.d_func
		return arch.EncodeName(fu.name)

	case DEF_Var:
		va := def.d_var
		return arch.EncodeName(va.name)

	default:
		panic("weird def")
	}
}

func (arch *AMD64_Arch) EncodeLabel(name string) string {
	name = arch.EncodeName(name)

	if name[0] == '$' {
		name = name[1:]
	}
	return "." + name
}

func (arch *AMD64_Arch) EncodeStack(r_name string, offset int) string {
	if offset == 0 {
		return fmt.Sprintf("[%s]", r_name)
	} else if offset < 0 {
		return fmt.Sprintf("[%s-%d]", r_name, 0 - offset)
	} else {
		return fmt.Sprintf("[%s+%d]", r_name, offset)
	}
}

func (arch *AMD64_Arch) EncodeName(name string) string {
	// encode identifiers to be compatible with NASM:
	//   1. ASCII letters, digits and the underscore stay the same
	//   2. escape '-' as "__"
	//   3. escape '?' as "_Q"
	//   4. escape ':' as "$"
	//   5. escape ASCII symbols/punctuation as "?XX" (two hex digits)
	//   6. escape unicode characters as "?uXXXX" or "?vXXXXXX"
	//   7. if solely ASCII, prefix with a '$' (which NASM removes)

	var sb strings.Builder
	has_escape := false

	for _, ch := range []rune(name) {
		if ch == '_' || ('A' <= ch && ch <= 'Z') || ('a' <= ch && ch <= 'z') || ('0' <= ch && ch <= '9') {
			sb.WriteByte(byte(ch))
		} else if ch > 0xFFFF {
			fmt.Fprintf(&sb, "?v%06X", ch)
			has_escape = true
		} else if ch >= 0x7F {
			fmt.Fprintf(&sb, "?u%04X", ch)
			has_escape = true
		} else if ch == ':' {
			sb.WriteString("$")
			has_escape = true
		} else if ch == '-' {
			sb.WriteString("__")
			has_escape = true
		} else if ch == '?' {
			sb.WriteString("_Q")
			has_escape = true
		} else {
			fmt.Fprintf(&sb, "?%02X", ch)
			has_escape = true
		}
	}

	// prevent words clashing with assembly instructions/registers/etc
	if !has_escape {
		return "$" + sb.String()
	}

	return sb.String()
}

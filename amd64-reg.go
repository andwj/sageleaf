// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

// import "strconv"
import "sort"

const (
	RAX Register = 1 + iota
	RBX
	RCX
	RDX

	RBP
	RSI
	RDI
	RSP

	R8
	R9
	R10
	R11
	R12
	R13
	R14
	R15

	XMM0
	XMM1
	XMM2
	XMM3
	XMM4
	XMM5
	XMM6
	XMM7

	XMM8
	XMM9
	XMM10
	XMM11
	XMM12
	XMM13
	XMM14
	XMM15
)

func (arch *AMD64_Arch) RegisterBits() int {
	return 64
}

func (arch *AMD64_Arch) RegName(reg Register) string {
	switch reg {
	case RAX: return "rax"
	case RBX: return "rbx"
	case RCX: return "rcx"
	case RDX: return "rdx"
	case RBP: return "rbp"
	case RSI: return "rsi"
	case RDI: return "rdi"
	case RSP: return "rsp"

	case R8:  return "r8"
	case R9:  return "r9"
	case R10: return "r10"
	case R11: return "r11"
	case R12: return "r12"
	case R13: return "r13"
	case R14: return "r14"
	case R15: return "r15"

	case XMM0: return "xmm0"
	case XMM1: return "xmm1"
	case XMM2: return "xmm2"
	case XMM3: return "xmm3"
	case XMM4: return "xmm4"
	case XMM5: return "xmm5"
	case XMM6: return "xmm6"
	case XMM7: return "xmm7"

	case XMM8:  return "xmm8"
	case XMM9:  return "xmm9"
	case XMM10: return "xmm10"
	case XMM11: return "xmm11"
	case XMM12: return "xmm12"
	case XMM13: return "xmm13"
	case XMM14: return "xmm14"
	case XMM15: return "xmm15"

	default:
		panic("RegName with bad register")
	}
}

func (arch *AMD64_Arch) RegName8(reg Register) string {
	switch reg {
	case RAX: return "al"
	case RBX: return "bl"
	case RCX: return "cl"
	case RDX: return "dl"
	case RBP: return "bpl"
	case RSI: return "sil"
	case RDI: return "dil"
	case RSP: return "spl"

	case R8:  return "r8b"
	case R9:  return "r9b"
	case R10: return "r10b"
	case R11: return "r11b"
	case R12: return "r12b"
	case R13: return "r13b"
	case R14: return "r14b"
	case R15: return "r15b"

	default:
		panic("RegName8 with bad register")
	}
}

func (arch *AMD64_Arch) RegName16(reg Register) string {
	switch reg {
	case RAX: return "ax"
	case RBX: return "bx"
	case RCX: return "cx"
	case RDX: return "dx"
	case RBP: return "bp"
	case RSI: return "si"
	case RDI: return "di"
	case RSP: return "sp"

	case R8:  return "r8w"
	case R9:  return "r9w"
	case R10: return "r10w"
	case R11: return "r11w"
	case R12: return "r12w"
	case R13: return "r13w"
	case R14: return "r14w"
	case R15: return "r15w"

	default:
		panic("RegName16 with bad register")
	}
}

func (arch *AMD64_Arch) RegName32(reg Register) string {
	switch reg {
	case RAX: return "eax"
	case RBX: return "ebx"
	case RCX: return "ecx"
	case RDX: return "edx"
	case RBP: return "ebp"
	case RSI: return "esi"
	case RDI: return "edi"
	case RSP: return "esp"

	case R8:  return "r8d"
	case R9:  return "r9d"
	case R10: return "r10d"
	case R11: return "r11d"
	case R12: return "r12d"
	case R13: return "r13d"
	case R14: return "r14d"
	case R15: return "r15d"

	default:
		// the floating point registers stay the same
		return arch.RegName(reg)
	}
}

func (arch *AMD64_Arch) RegNameSize(reg Register, bits int) string {
	switch bits {
	case 8:  return arch.RegName8(reg)
	case 16: return arch.RegName16(reg)
	case 32: return arch.RegName32(reg)
	default: return arch.RegName(reg)
	}
}

//----------------------------------------------------------------------

func (arch *AMD64_Arch) ParameterRegs(par_types []*Type) []Register {
	list := make([]Register, 0)

	i_num := 0
	f_num := 0

	for n, ty := range par_types {
		is_float := (ty.kind == TYP_Float)
		is_int   := !is_float

		reg := REG_ON_STACK

		if arch.abi == "win" {
			// Win64 ABI

			if n < 4 && is_float {
				reg = XMM0 + Register(n)

			} else if n < 4 && is_int {
				switch n {
				case 0: reg = RCX
				case 1: reg = RDX
				case 2: reg = R8
				case 3: reg = R9
				}
			}

		} else {
			// System V AMD64 ABI -- for BSDs, Linux, MacOS

			if is_float && f_num < 8 {
				reg = XMM0 + Register(f_num)
				f_num += 1

			} else if is_int && i_num < 6 {
				switch i_num {
				case 0: reg = RDI
				case 1: reg = RSI
				case 2: reg = RDX
				case 3: reg = RCX
				case 4: reg = R8
				case 5: reg = R9
				}
				i_num += 1
			}
		}

		list = append(list, reg)
	}

	return list
}

//----------------------------------------------------------------------

type AMD64_RegAssign struct {
	group map[*LocalVar]Register
	cost  int64

	extra_int   int
	extra_float int
}

func (arch *AMD64_Arch) RegisterAllocate() error {
	arch.RA_Parameters()
	arch.RA_CostOfLocals()

	// try various combinations of "extra registers" (ones which will
	// need to be saved at function entry and restored at exit) and
	// find the most optimal combination.

	max_int   := 5  // RBX and R12..R15
	max_float := 0

	if arch.abi == "win" {
		max_int   = 7  // include RSI and RDI
		max_float = 8  // XMM8..XMM15
	}

	best     := arch.RA_AllocateWith(0, 0)
	best_int := 0

	for extra_int := 1; extra_int <= max_int; extra_int++ {
		asg := arch.RA_AllocateWith(extra_int, 0)
		if asg.cost < best.cost {
			best = asg
			best_int = extra_int
		}
	}

	for extra_float := 1; extra_float <= max_float; extra_float++ {
		asg := arch.RA_AllocateWith(best_int, extra_float)
		if asg.cost < best.cost {
			best = asg
		}
	}

	// update the locals with the best allocation
	for _, local := range arch.ordered_locals {
		if local.param_idx >= 0 && local.reads == 0 {
			// don't waste a stack slot on an unused parameter
			local.reg = 0
		} else {
			local.reg = REG_ON_STACK
		}
	}
	for local, reg := range best.group {
		local.reg = reg

		// callee-saved registers need to be stored on the stack
		if arch.RA_IsExtra(reg) {
			arch.extra_regs.Add(reg)
		}
	}

	return OK
}

func (arch *AMD64_Arch) RA_Parameters() {
	abi_regs := arch.ParameterRegs(arch.fu.par_types)

	// need to skip the saved RBP and the return address
	stack_offset := 16

	for n := 0 ; n < arch.fu.params ; n++ {
		par := arch.fu.locals[n]
		par.abi_reg = abi_regs[n]

		if par.abi_reg == REG_ON_STACK || (arch.abi == "win" && n < 4) {
			par.offset = stack_offset
			stack_offset += 8
		}
	}
}

func (arch *AMD64_Arch) RA_CostOfLocals() {
	// create list of locals sorted by cost, highest first
	arch.costed_locals = make([]*LocalVar, 0)

	for _, local := range arch.fu.locals {
		if local.reads > 0 {
			local.CalcCost(arch.c_create, arch.c_write, arch.c_read)
			arch.costed_locals = append(arch.costed_locals, local)
		}
//		println(arch.fu.name, local.name, "=", local.cost)
	}

	sort.Slice(arch.costed_locals, func(i, k int) bool {
		L1 := arch.costed_locals[i]
		L2 := arch.costed_locals[k]

		if L1.cost != L2.cost {
			return L1.cost > L2.cost
		}

		// secondary test is by order
		return L1.OrdLess(L2)
	})

//	println("FUNCTION:", arch.fu.name)
//	for _, local := range arch.costed_locals {
//		println("  ", local.name, local.cost)
//	}
}

func (arch *AMD64_Arch) RA_AllocateWith(extra_int, extra_float int) *AMD64_RegAssign {
	asg := new(AMD64_RegAssign)
	asg.group = make(map[*LocalVar]Register)
	asg.extra_int = extra_int
	asg.extra_float = extra_float

	if Options.no_optim {
		return asg
	}

	// visit each local, try to find a register for it
	for _, local := range arch.costed_locals {
		arch.RA_VisitLocal(local, asg)
	}

	// total cost is cost of save/restore the extra registers plus the
	// costs of each local.
	save_cost := int64(arch.c_create + arch.c_write + arch.c_read)

	asg.cost += save_cost * int64(extra_int + extra_float)

	for _, local := range arch.fu.locals {
		if local.reads > 0 {
			_, has_reg := asg.group[local]
			if !has_reg {
				asg.cost += local.cost
			}
		}
	}

/* DEBUG
println("TOTAL COST OF", arch.fu.name, extra_int, extra_float, "=", asg.cost)
println("allocated regs:", len(asg.group))
*/
	return asg
}

func (arch *AMD64_Arch) RA_VisitLocal(local *LocalVar, asg *AMD64_RegAssign) {
	prefer := arch.RA_PreferRegister(local)

	var reg_list []Register

	if local.ty.kind == TYP_Float {
		reg_list = arch.RA_FloatRegisters(asg.extra_float, prefer)
	} else {
		reg_list = arch.RA_IntRegisters(asg.extra_int, prefer)
	}

	clobber := arch.RA_ClobberSet(local, asg)

	for _, reg := range reg_list {
		if !clobber.Has(reg) {
			// use this register for the local
			asg.group[local] = reg
			break
		}
	}
}

func (arch *AMD64_Arch) RA_IntRegisters(extra int, prefer Register) []Register {
	list := make([]Register, 0)

	if prefer != 0 {
		list = append(list, prefer)
	}

	for reg := R12; reg <= R15; reg++ {
		if extra > 0 {
			list = append(list, reg)
			extra--
		}
	}
	if extra > 0 {
		list = append(list, RBX)
		extra--
	}
	if arch.abi == "win" {
		if extra > 0 {
			list = append(list, RSI)
			extra--
		}
		if extra > 0 {
			list = append(list, RDI)
			extra--
		}
	}

	// ordinary (clobbered by func call) registers

	list = append(list, R10)
	list = append(list, R9)
	list = append(list, R8)
	list = append(list, RCX)
	list = append(list, RDX)

	if arch.abi != "win" {
		list = append(list, RSI)
		list = append(list, RDI)
	}

	return list
}

func (arch *AMD64_Arch) RA_FloatRegisters(extra int, prefer Register) []Register {
	list := make([]Register, 0)

	if prefer != 0 {
		list = append(list, prefer)
	}

	if arch.abi == "win" {
		for i := 0; i < extra; i++ {
			list = append(list, XMM8 + Register(i))
		}
		for reg := XMM0; reg < XMM4; reg++ {
			list = append(list, reg)
		}
	} else {
		for reg := XMM0; reg < XMM14; reg++ {
			list = append(list, reg)
		}
	}

	return list
}

func (arch *AMD64_Arch) RA_IsExtra(reg Register) bool {
	switch reg {
	case RBX, R12, R13, R14, R15:
		return true
	}

	if arch.abi == "win" {
		switch reg {
		case RSI, RDI:
			return true
		}
		if reg >= XMM6 {
			return true
		}
	}

	return false
}

func (arch *AMD64_Arch) RA_PreferRegister(local *LocalVar) Register {
	// determine what could be the ideal register for this local.
	// this is most important for temporaries (single use locals).

	if local.param_idx >= 0 {
		if local.abi_reg != REG_ON_STACK {
			return local.abi_reg
		}
	}

	fu    := arch.fu
	nodes := fu.body.children

	for i := local.live_start + 1; i <= local.live_end; i++ {
		t    := nodes[i]
		comp := GetComputation(t)

		var t_call *Node

		if comp != nil {
			if comp.kind == NC_Call || comp.kind == NC_TailCall {
				t_call = comp
			}
		}

		if t_call != nil {
			par_types := ParamTypesInCall(t_call)
			par_regs  := arch.ParameterRegs(par_types)

			for i, par := range t_call.children[1:] {
				if par.kind == NP_Local && par.local == local {
					if par_regs[i] != REG_ON_STACK {
						return par_regs[i]
					}
				}
			}
		}
	}

	return 0  // none
}

func (arch *AMD64_Arch) RA_ClobberSet(local *LocalVar, asg *AMD64_RegAssign) *RegisterSet {
	// determine registers which cannot be used for a particular local,
	// including cases where a particular register is forced by a particular
	// instruction (e.g. RDX is needed by the IDIV instruction).

	set := NewRegisterSet()

	if local.param_idx >= 0 {
		abi_regs := arch.ParameterRegs(arch.fu.par_types)

		for reg := RAX; reg <= XMM15; reg++ {
			if arch.RA_ParamConflict(local, reg, abi_regs) {
				set.Add(reg)
			}
		}
	}

	for i := local.live_start; i <= local.live_end; i++ {
		arch.RA_ClobbersForNode(i, local, set)
	}

	for other, reg := range asg.group {
		if other.live_start >= local.live_end ||
			other.live_end <= local.live_start {
			// no overlap
		} else {
			set.Add(reg)
		}
	}

	return set
}

func (arch *AMD64_Arch) RA_ClobbersForNode(idx int, local *LocalVar, clobber *RegisterSet) {
	// WISH: an assignment to a local can mean previous nodes which do not
	//       need the local's value (AND no labels in between) don't really
	//       clobber it...

	nodes := arch.fu.body.children
	t     := nodes[idx]

	// first use creates the local, it can never clobber it
	if idx <= local.live_start {
		return
	}

	// nodes which assign to the local can be ignored here
	if t.kind == NH_Move {
		dest := t.children[0]
		if dest.kind == NP_Local && dest.local == local {
			return
		}
	}

	// the last usage generally does not matter, local won't be used again.
	// [ except for loops, but ExtendLiveRanges takes care of that ]
	// main exception is when local is a pointer and node stores to that
	// pointer -- clearly the local must survive the computation then.

	is_last  := (idx >= local.live_end)
	is_deref := arch.StoresToDerefLocal(t, local)

	comp := GetComputation(t)

	// tail calls are ignored here, since they never return.
	// WISH: ignore a call to a `no-return` function too.

	if comp != nil && comp.kind == NC_Call {
		if is_last && !is_deref {
			return
		}

		clobber.Add(RCX)
		clobber.Add(RDX)
		clobber.Add(R8)
		clobber.Add(R9)
		clobber.Add(R10)
		clobber.Add(R11)

		for r := XMM0; r <= XMM5; r++ {
			clobber.Add(r)
		}

		if arch.abi != "win" {
			clobber.Add(RDI)
			clobber.Add(RSI)

			for r := XMM6; r <= XMM15; r++ {
				clobber.Add(r)
			}
		}
	}

	if comp != nil && comp.kind == NC_OpSeq {
		// visit each NCX_Operator
		for i := 1; i < comp.Len(); i++ {
			op := comp.children[i]

			switch op.str {
			case "fremt":
				// remainder calc requires a third register
				clobber.Add(XMM3)

			case "idivt", "udivt", "iremt", "uremt":
				// DIV instructions require the RAX and RDX registers
				clobber.Add(RDX)

			case "ishl", "ishr", "ushl", "ushr":
				// shift instructions need the RCX register (unless shift is a constant)
				t_arg := comp.children[0]

				if t_arg.kind == NL_Integer {
					// ok
				} else if t_arg.kind == NP_Local && t_arg.local == local {
					// ok
				} else {
					clobber.Add(RCX)
				}
			}
		}
	}

	if t.kind == NH_Swap {
		t_left  := t.children[0]
		t_right := t.children[1]

		// may need a register if one operand is a memory access
		if t_left.kind == NP_Memory || t_right.kind == NP_Memory {
			if t_left.ty.kind != TYP_Float {
				clobber.Add(R10)
			}
		}
	}
}

func (arch *AMD64_Arch) RA_ParamConflict(par *LocalVar, reg Register, abi_regs []Register) bool {
	// when a parameter is allocated a register, there is code generated
	// at the beginning of a function to transfer it, but that code could
	// overwrite another (in-reg) parameter.  we check for that here.
	//
	// for example: if our function takes parameters x in XMM0 and y in XMM1,
	// then x cannot be allocated to XMM1 since the transfer would overwrite
	// parameter y.

	// if the parameter is on the stack, it will always be transferred
	// after every in-reg parameter of the same kind (int or float) has
	// been transferred, so no chance of overwrite.
	if par.abi_reg == REG_ON_STACK {
		return false
	}

	// no problem when it stays where it is
	if par.abi_reg == reg {
		return false
	}

	// no problem if the register is not used by any other parameter
	used := false
	for _, other := range abi_regs {
		if other == reg {
			used = true
			break
		}
	}
	if !used {
		return false
	}

	// floating point register?
	if par.abi_reg >= XMM0 {
		return reg >= par.abi_reg
	}

	// for integer registers, categorise into a few groups
	// (to keep the logic simple and ABI-agnostic).
	var p int
	var r int

	switch par.abi_reg {
	case RDI, RSI: p = 0
	case RCX, RDX: p = 1
	case R8:       p = 2
	case R9:       p = 3
	}

	switch reg {
	case RDI, RSI: r = 0
	case RCX, RDX: r = 1
	case R8:       r = 2
	case R9:       r = 3
	default: return false
	}

	return r >= p
}

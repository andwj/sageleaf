// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "fmt"
import "bufio"

type FileInfo struct {
	in_name  string
	in_fp    *os.File
	lexer    *Lexer

	out_name    string
	out_fp      *os.File
	out_errors  bool

	// current visibility (default is private)
	public  bool
}

var cur_file *FileInfo

//----------------------------------------------------------------------

func ProcessFile(f_idx int, fullname string) error {
	InitDefs()

	if LoadCodeFile(f_idx, fullname) != OK {
		return FAILED
	}

	CreateOrderedDefs()

	if ParseEverything() != OK {
		return FAILED
	}

	InlineFunctions()

	if CreateOutputFile() != OK {
		return FAILED
	}

	CompileEverything()

	cur_file.CloseOutput()

	return OK
}

func CompileEverything() {
	arch.Begin()

	for _, def := range ord_defs {
		arch.Generate(def)
	}

	arch.Finish()
}

//----------------------------------------------------------------------

func LoadCodeFile(f_idx int, fullname string) error {
	read_fp, err := os.Open(fullname)

	if err != nil {
		Error_SetFile(0)

		if os.IsNotExist(err) {
			PostError("no such file: %s", fullname)
		} else {
			PostError("could not open file: %s", fullname)
		}
		return FAILED
	}

	cur_file = NewFileInfo(fullname, f_idx, read_fp)
	cur_file.Load()
	cur_file.CloseInput()

	if have_errors {
		return FAILED
	}
	return OK
}

func CreateOutputFile() error {
	f := cur_file

	bare := FileRemoveExtension(f.in_name)
	ext  := arch.OutputExt()

	f.out_name = bare + ext

	var err error
	f.out_fp, err = os.Create(f.out_name)

	if err != nil {
		FatalError("cannot create output: %s", err.Error())
	}
	return OK
}

func OutLine(format string, a ...interface{}) {
	if cur_file == nil {
		panic("OutLine with no destination")
	}

	if cur_file.out_errors {
		return
	}

	format += "\n"

	_, err := fmt.Fprintf(cur_file.out_fp, format, a...)
	if err != nil {
		cur_file.out_errors = true
	}
}

func OutIns(ins, args string, a ...interface{}) {
	line := "\t" + ins

	if args != "" {
		line += "\t" + fmt.Sprintf(args, a...)
	}
	OutLine("%s", line)
}

//----------------------------------------------------------------------

func NewFileInfo(name string, filenum int, fp *os.File) *FileInfo {
	reader := bufio.NewReader(fp)
	lexer  := NewLexer(reader, filenum)

	f := new(FileInfo)

	f.in_name = name
	f.in_fp   = fp
	f.public  = false
	f.lexer   = lexer

	return f
}

func (f *FileInfo) CloseInput() {
	f.in_fp.Close()
	f.in_fp = nil
	f.lexer = nil
}

func (f *FileInfo) CloseOutput() {
	f.out_fp.Close()
	f.out_fp = nil
}

// Load reads the input file, decodes each line into low-level tokens
// and does some awesome shit.  Returns OK if everything was fine, or
// FAILED if an error occurred.
func (f *FileInfo) Load() error {
	for {
		t := f.lexer.Scan()

		if t.kind == NL_EOF {
			if have_errors {
				return FAILED
			}
			return OK
		}

		// NOTE: on parse failures we generally skip ahead to the next
		//       known directive, allowing multiple syntax errors to be
		//       found and reported.

		Error_SetPos(t.pos)

		if t.kind == NL_ERROR {
			PostError("%s", t.str)
			f.lexer.SkipAhead()
			continue
		}

		head := t.children[0]

		if head.kind != NL_Name {
			PostError("expected a directive, got: %s", t.String())
			f.lexer.SkipAhead()
			continue
		}

		switch {
		case head.Match("var"):
			if f.LoadVar(t) != OK {
				f.lexer.SkipAhead()
			}

		case head.Match("fun"):
			if f.LoadFunction(t) != OK {
				f.lexer.SkipAhead()
			}

		case head.Match("extern"):
			if f.LoadExtern(t) != OK {
				f.lexer.SkipAhead()
			}

		case head.Match("public"):
			f.public = true

		case head.Match("private"):
			f.public = false

		case head.Match("end"):
			PostError("end directive outside a function or variable")
			f.lexer.SkipAhead()

		default:
			PostError("unknown directive: %s", head.str)
			f.lexer.SkipAhead()
		}
	}
}

func (f *FileInfo) Load_DEBUG() error {
	for {
		t := f.lexer.Scan()

		Error_SetPos(t.pos)

		if t.kind == NL_ERROR {
			println("** ERROR IN LINE", t.pos.line, ":", t.str)
			return OK
		}

		if t.kind == NL_EOF {
			println("** EOF **")
			return OK
		}

		Dump(" ", t, 0)
	}
}

//----------------------------------------------------------------------

func (f *FileInfo) LoadExtern(line *Node) error {
	children := line.children

	if len(children) < 2 {
		PostError("missing name after extern directive")
		return FAILED
	}
	if len(children) > 2 {
		PostError("extra rubbish after extern directive")
		return FAILED
	}

	t_name := children[1]

	if ValidateName(t_name, "extern", '@') != OK {
		return FAILED
	}

	def := AddExtern(t_name.str)
	_ = def

	return OK
}

//----------------------------------------------------------------------

func (f *FileInfo) LoadVar(line *Node) error {
	children := line.children

	if len(children) < 2 {
		PostError("variable is missing name")
		return FAILED
	}

	is_rom := false
	if children[1].Match("rom") {
		is_rom = true
		children = children[1:]
	}

	if len(children) < 3 {
		PostError("missing '=' after variable name")
		return FAILED
	}

	t_name  := children[1]
	t_equal := children[2]
	children = children[3:]

	if ! t_equal.Match("=") {
		PostError("missing '=' after variable name")
		return FAILED
	}

	if ValidateName(t_name, "variable", '@') != OK {
		return FAILED
	}

	def := AddVar(t_name.str)
	va  := def.d_var

	va.rom    = is_rom
	va.public = f.public

	if len(children) == 0 {
		// handle a value which occupies multiple lines.
		// we don't know the exact type yet, so this form is allowed
		// for all types, even if it is a bit silly for one integer.

		va.body = NewNode(NG_Block, "", line.pos)
		return f.LoadBody(va.body, "variable")
	}

	va.body = NewNode(NG_Line, "", line.pos)
	for _, child := range children {
		va.body.Add(child)
	}

	return OK
}

//----------------------------------------------------------------------

func (f *FileInfo) LoadFunction(line *Node) error {
	children := line.children

	if len(children) < 3 {
		PostError("function is missing name or parameters")
		return FAILED
	}
	if len(children) > 3 {
		PostError("extra rubbish after function parameters")
		return FAILED
	}

	t_name  := children[1]
	t_param := children[2]

	if ValidateName(t_name, "function", '@') != OK {
		return FAILED
	}

	def := AddFunc(t_name.str)
	fu  := def.d_func

	fu.name   = t_name.str
	fu.public = f.public

	if fu.ParseParameters(t_param) != OK {
		return FAILED
	}

	fu.body = NewNode(NG_Block, "", line.pos)
	return f.LoadBody(fu.body, "function")
}

func (fu *FuncDef) ParseParameters(param *Node) error {
	fu.params    = 0
	fu.par_types = make([]*Type, 0)
	fu.ret_type  = nil

	children := param.children

	for len(children) > 0 {
		if fu.ret_type != nil {
			PostError("extra rubbish after return type")
			return FAILED
		}

		if len(children) < 2 {
			PostError("malformed parameters")
			return FAILED
		}

		t_name := children[0]

		if t_name.Match("->") {
			fu.ret_type = ParseType(children[1])
			if fu.ret_type == nil {
				return FAILED
			}

			children = children[2:]
			continue
		}

		if ValidateName(t_name, "parameter", '%') != OK {
			return FAILED
		}

		par_type := ParseType(children[1])
		if par_type == nil {
			return FAILED
		}

		if fu.FindLocal(t_name.str) != nil {
			PostError("duplicate parameter name: %%%s", t_name.str)
			return FAILED
		}

		fu.AddParameter(t_name.str, par_type)

		children = children[2:]
	}

	if fu.ret_type == nil {
		PostError("missing return type in parameter list")
		return FAILED
	}

	return OK
}

//----------------------------------------------------------------------

func (f *FileInfo) LoadBody(body *Node, what string) error {
	is_unclosed := false

	for {
		t := f.lexer.Scan()

		if t.kind == NL_EOF {
			PostError("unfinished %s (reached end of file)", what)
			return FAILED
		}

		Error_SetPos(t.pos)

		if t.kind == NL_ERROR {
			PostError("%s", t.str)
			return FAILED
		}

		head := t.children[0]

		if head.Match("end") {
			if t.Len() > 1 {
				PostError("extra rubbish after 'end' directive")
				return FAILED
			}
			if is_unclosed {
				return FAILED
			}
			return OK
		}

		if head.Match("fun") || head.Match("var") {
			if ! is_unclosed {
				Error_SetPos(body.pos)
				PostError("%s body is unclosed or missing", what)
				is_unclosed = true
			}
		}

		// we don't do any analysis of the code/data here
		body.Add(t)
	}
}

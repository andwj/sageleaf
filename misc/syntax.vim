" Vim syntax file
" Language:    SAGELEAF
" Maintainer:  Andrew Apted  <ajapted@users.sf.net>
" Last Change: 2022 Aug 10
" Home Page:   https://gitlab.com/andwj/sageleaf

" quit when a syntax file was already loaded
if exists("b:current_syntax")
  finish
endif

" syntax is case sensitive
syn case match

" identifier characters
setlocal iskeyword=33,36-39,42-43,45-46,48-57,60-90,95,97-122,126,_

" comments
syn region  sageleaf_Comment       start=/;/ end=/$/ contains=sageleaf_CommentTodo keepend
syn keyword sageleaf_CommentTodo   contained TODO FIXME XXX

" plain identifiers
syn match sageleaf_Identifier      /\k\+/

" sigil'd identifiers
syn match sageleaf_Global          /@\k*/
syn match sageleaf_Local           /%\k*/
syn match sageleaf_Label           /^\k*:\s*$/

" strings and characters
syn region sageleaf_String         start=/"/ skip=/\\\\\|\\"/ end=/"/
syn region sageleaf_String         start=/'/ skip=/\\\\\|\\'/ end=/'/

" integers
syn match sageleaf_Number          /[+-]\?\d\+/
syn match sageleaf_Number          /[+-]\?0b[01]\+/
syn match sageleaf_Number          /[+-]\?0x\x\+/

" floating point
syn match sageleaf_Number          /[+-]\?\d\+[.]\d\+/
syn match sageleaf_Number          /[+-]\?\d\+[.]\d*[eE][+-]\?\d\+/
syn match sageleaf_Number          /[+-]\?\d\+[eE][+-]\?\d\+/
syn match sageleaf_Number          /[+-]\?0x\x\+[.]\x*[pP][+-]\?\d\+/
syn match sageleaf_Number          /[+-]\?0x\x\+[pP][+-]\?\d\+/

" types
syn keyword sageleaf_Type          i8 i16 i32 i64
syn keyword sageleaf_Type          f32 f64
syn keyword sageleaf_Type          ptr iptr
syn keyword sageleaf_Type          void bool

" special directives
syn keyword sageleaf_Special       public private

" language keywords
syn keyword sageleaf_Keyword       var stack-var rom zero
syn keyword sageleaf_Keyword       fun inline tail no-return
syn keyword sageleaf_Keyword       extern end file line
syn keyword sageleaf_Keyword       call swap jump if ret
syn keyword sageleaf_Keyword       panic check-null check-range
syn keyword sageleaf_Keyword       matches? not

" constants
syn keyword sageleaf_Constant      +INF -INF QNAN SNAN

" unary operations
syn keyword sageleaf_Operation     ineg iabs iflip
syn keyword sageleaf_Operation     fneg fabs fsqrt
syn keyword sageleaf_Operation     izero? isome? ineg? ipos?
syn keyword sageleaf_Operation     fzero? fsome? fneg? fpos?
syn keyword sageleaf_Operation     fnan? finf?
syn keyword sageleaf_Operation     null? ref?
syn keyword sageleaf_Operation     iswap ilittle ibig
syn keyword sageleaf_Operation     bnot

" binary operations
syn keyword sageleaf_Operation     iadd isub iand ior ixor
syn keyword sageleaf_Operation     imul idivt udivt iremt uremt
syn keyword sageleaf_Operation     ishl ishr ushl ushr
syn keyword sageleaf_Operation     imax imin umax umin

syn keyword sageleaf_Operation     fadd fsub fmul fdiv fremt
syn keyword sageleaf_Operation     fmax fmin
syn keyword sageleaf_Operation     fround ftrunc ffloor fceil

syn keyword sageleaf_Operation     padd psub pdiff pmin pmax
syn keyword sageleaf_Operation     band bor  bxor

syn keyword sageleaf_Operation     ieq? ine? ilt? ile? igt? ige?
syn keyword sageleaf_Operation     ueq? une? ult? ule? ugt? uge?
syn keyword sageleaf_Operation     peq? pne? plt? ple? pgt? pge?
syn keyword sageleaf_Operation     feq? fne? flt? fle? fgt? fge?
syn keyword sageleaf_Operation     beq? bne? blt? ble? bgt? bge?

" conversion operations
syn keyword sageleaf_Operation     itof utof ftoi ftou btoi
syn keyword sageleaf_Operation     iext uext fext raw-cast

" Define the default highlighting.
" Only when an item doesn't have highlighting yet

hi def link sageleaf_Comment       Comment
hi def link sageleaf_CommentTodo   Todo
hi def link sageleaf_Constant      Constant
hi def link sageleaf_Global        Normal
hi def link sageleaf_Identifier    Normal
hi def link sageleaf_Keyword       Keyword
hi def link sageleaf_Label         Special
hi def link sageleaf_Local         Normal
hi def link sageleaf_Number        Number
hi def link sageleaf_NumberError   Error
hi def link sageleaf_NumberFloat   Float
hi def link sageleaf_Operation     Keyword
hi def link sageleaf_Special       Special
hi def link sageleaf_String        String
hi def link sageleaf_StringError   Error
hi def link sageleaf_StringEscape  SpecialChar
hi def link sageleaf_Type          Type

let b:current_syntax = "sageleaf"

" vim:ts=8:sw=8:noexpandtab:nowrap

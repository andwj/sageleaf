// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
import "strconv"
import "sort"

// AMD64_Arch is an implementation of the Architecture interface
// which generate output assembly for the AMD64 cpu architecture.
type AMD64_Arch struct {
	cur_section string

	// the ABI is either "win" or "sysv"
	abi string

	// floating point constants get stored in the .text section,
	// and these remember the offset into an array.
	f32_consts map[string]int
	f64_consts map[string]int

	// some optimisation constants
	c_create int
	c_write  int
	c_read   int

	// floating point work registers
	workf1 Register
	workf2 Register

	// compiling info for a single function....
	fu *FuncDef

	extra_regs *RegisterSet

	ordered_locals []*LocalVar  // sorted by appearance
	costed_locals  []*LocalVar  // sorted by cost

	file_def  *Definition  // var containing current filename
	line_num  int

	need_end    bool

	new_labels  int
}

//----------------------------------------------------------------------

func NewAMD64Arch() *AMD64_Arch {
	arch := new(AMD64_Arch)

	if Options.os == "windows" {
		arch.abi = "win"
	} else {
		arch.abi = "sysv"
	}

	if arch.abi == "win" {
		// Win64 ABI cannot use XMM14/15 since they are in the set of
		// callee-saved registers (non-clobbered by function calls).
		arch.workf1 = XMM4
		arch.workf2 = XMM5
	} else {
		// SysV ABI cannot use XMM4/5 since they may be used to pass
		// parameters in a function call.
		arch.workf1 = XMM14
		arch.workf2 = XMM15
	}

	arch.c_create = 200
	arch.c_write  = 24
	arch.c_read   = 10

	return arch
}

func (arch *AMD64_Arch) OutputExt() string {
	return ".s"
}

func (arch *AMD64_Arch) Begin() {
	arch.f32_consts = make(map[string]int)
	arch.f64_consts = make(map[string]int)
}

func (arch *AMD64_Arch) Finish() {
	arch.Section("text")

	OutLine("extern panic")
	OutLine("extern panic_null")
	OutLine("extern panic_range")
	OutLine("")

	arch.WriteFloats()

	// this makes viewing with vim easier
	OutLine("")
	OutLine("; vi:ts=10:sw=10")
}

// Section outputs assembly to set the current section.
// The parameter must be one of "text", "data" and "bss" for now.
func (arch *AMD64_Arch) Section(sec string) {
	if arch.cur_section != sec {
		arch.cur_section = sec

		OutLine("section ." + sec)
		OutLine("")
	}
}

func (arch *AMD64_Arch) BigEndian() bool {
	return false
}

func (arch *AMD64_Arch) Generate(def *Definition) {
	switch def.kind {
	case DEF_Var:    arch.GenerateVar   (def.d_var)
	case DEF_Func:   arch.GenerateFunc  (def.d_func)
	case DEF_Extern: arch.GenerateExtern(def.d_extern)
	default: panic("weird def")
	}
}

//----------------------------------------------------------------------

func (arch *AMD64_Arch) GenerateFunc(fu *FuncDef) {
	arch.fu = fu

	arch.extra_regs  = NewRegisterSet()
	arch.ordered_locals = nil
	arch.costed_locals  = nil
	arch.file_def    = nil
	arch.line_num    = 0
	arch.need_end    = false
	arch.new_labels  = 0

	arch.Section("text")

	out_name := arch.EncodeName(fu.name)

	if fu.public {
		OutLine("global %s", out_name)
	}

	OutLine("%s:", out_name)

/* TODO optims
	arch.fu.DeadCodeRemoval()
	arch.fu.UnusedLocals()
	arch.fu.StrengthReduce()
	arch.fu.UnusedLocals()
*/

//	Dumpty(arch.fu.name, arch.fu.body, 0)

	arch.fu.DetectLoops()
	arch.fu.LiveRanges(false)
	arch.fu.ExtendLiveRanges()

	// collect and sort locals, to give a reproducible order
	arch.SortLocals()

	if arch.RegisterAllocate() != OK {
		return
	}

	arch.SetupStack()
	arch.SaveExtraRegs()
	arch.TransferParameters()

	OutLine("")

	for _, t := range fu.body.children {
		arch.GenerateNode(t)
	}

	ret_type := arch.fu.ret_type

	if ret_type.kind != TYP_Void {
		// since `no-return` cannot return, we will raise an error.
		// do the same for any non-void return type, just in case our
		// detection of missing returns fails to catch it.
		OutIns("hlt", "")
	}

	if arch.need_end {
		OutLine(".__end:")
	}

	arch.RestoreExtraRegs()

	OutIns("leave", "")
	OutIns("ret", "")
	OutLine("")
}

func (arch *AMD64_Arch) SetupStack() {
	st := NewStackLayout(arch.fu)

	st.SA_Alloc(8 * arch.extra_regs.Len(), 8)

	StackAllocate(st)

	OutIns("push", "rbp")
	OutIns("mov", "rbp,rsp")

	if st.used > 0 {
		// ensure a 16-byte alignment of final stack base
		st.SA_Alloc(0, 16)

		OutIns("sub", "rsp,%d", st.used)
	}
}

func (arch *AMD64_Arch) SaveExtraRegs() {
	slot := 1

	for reg := RAX; reg <= R15; reg++ {
		if arch.extra_regs.group[reg] {
			OutIns("mov", "[rbp-%d],%s", slot*8, arch.RegName(reg))
			slot += 1
		}
	}
	for reg := XMM0; reg <= XMM15; reg++ {
		if arch.extra_regs.group[reg] {
			OutIns("movq", "[rbp-%d],%s", slot*8, arch.RegName(reg))
			slot += 1
		}
	}
}

func (arch *AMD64_Arch) RestoreExtraRegs() {
	slot := 1

	for reg := RAX; reg <= R15; reg++ {
		if arch.extra_regs.group[reg] {
			OutIns("mov", "%s,[rbp-%d]", arch.RegName(reg), slot*8)
			slot += 1
		}
	}
	for reg := XMM0; reg <= XMM15; reg++ {
		if arch.extra_regs.group[reg] {
			OutIns("movq", "%s,[rbp-%d]", arch.RegName(reg), slot*8)
			slot += 1
		}
	}
}

func (arch *AMD64_Arch) TransferParameters() {
	for n := 0 ; n < arch.fu.params ; n++ {
		par := arch.fu.locals[n]

		is_float := (par.ty.kind == TYP_Float)

		// ignore unused parameters
		if par.reads == 0 {
			continue
		}

		// it is where it should be?
		if par.abi_reg == par.reg {
			continue
		}

		ins := "mov"
		if is_float {
			ins = arch.FloatIns("movd", "movq", par.ty)
		}

		if par.reg == REG_ON_STACK {
			// register --> stack
			r_name   := arch.RegNameSize(par.abi_reg, par.ty.bits)
			slot_str := arch.EncodeStack("rbp", par.offset)
			OutIns(ins, "%s,%s", slot_str, r_name)

		} else if par.abi_reg == REG_ON_STACK {
			// stack --> register
			slot_str := arch.EncodeStack("rbp", par.offset)
			r_name   := arch.RegNameSize(par.reg, par.ty.bits)
			OutIns(ins, "%s,%s", r_name, slot_str)

		} else {
			// register --> register
			if is_float {
				ins = arch.FloatIns("movss", "movsd", par.ty)
			}
			r_dest := arch.RegNameSize(par.reg,     par.ty.bits)
			r_src  := arch.RegNameSize(par.abi_reg, par.ty.bits)
			OutIns(ins, "%s,%s", r_dest, r_src)
		}
	}
}

//----------------------------------------------------------------------

func (arch *AMD64_Arch) GenerateNode(t *Node) {
	switch t.kind {
	case NH_File:
		arch.file_def = t.def
		return

	case NH_Comment:
		arch.DoComment(t)
		return

	case NH_Label:
		arch.DoLabel(t)
		return

	case NH_Drop:
		arch.DoDrop(t)

	case NH_Move:
		arch.DoMove(t)

	case NH_Swap:
		arch.DoSwap(t)

	case NH_Jump:
		arch.DoJump(t)

	case NH_Return:
		arch.DoReturn(t)

	case NH_Panic:
		arch.DoPanic(t)

	case NH_ChkNull:
		arch.DoCheckNull(t)

	case NH_ChkRange:
		arch.DoCheckRange(t)

	default:
		panic("unknown node kind in GenerateNode")
	}

	OutLine("")
}

func (arch *AMD64_Arch) DoLabel(t *Node) {
	out_name := arch.EncodeLabel(t.str)
	OutLine("%s:", out_name)
}

func (arch *AMD64_Arch) DoComment(t *Node) {
	if t.offset > 0 {
		arch.line_num = int(t.offset)
	}

	if t.str != "" {
		OutLine("\t; %s", t.str)
	}
}

func (arch *AMD64_Arch) DoDrop(t *Node) {
	t_comp := t.children[0]

	arch.CalcComputation(t_comp, REG_NONE)
}

func (arch *AMD64_Arch) DoMove(t *Node) {
	t_dest := t.children[0]
	t_src  := t.children[1]

	// convert `x = iadd x 1` to an INC instruction
	if t_src.kind == NC_OpSeq && t_src.Len() == 2 &&
		arch.SameLocalOrMem(t_src.children[0], t_dest) {

		/* TODO
		op := t_src.children[1]

		if op.Len() == 1 && LiteralIsOne(op.children[0]) {
			switch op.str {
			case "iadd", "padd":
				dest := arch.ArgumentString(t_dest, 8|4)
				OutIns("inc", "%s", dest)
				return

			case "isub", "psub":
				dest := arch.ArgumentString(t_dest, 8|4)
				OutIns("dec", "%s", dest)
				return
			}
		}
		*/
	}

	// when source or dest is a register, we don't need an intermediate.
	if t_src.IsRegister() {
		reg := t_src.GetRegister()
		arch.StoreReg(reg, t_dest)
		return
	}
	if t_dest.IsRegister() {
		reg := t_dest.GetRegister()
		arch.CalcComputation(t_src, reg)
		return
	}

	// general case needs two instructions (a load and a store),
	// so determine the intermediate register....

	reg := RAX
	if t_dest.ty.kind == TYP_Float {
		reg = arch.workf1
	}

	arch.CalcComputation(t_src, reg)
	arch.StoreReg(reg, t_dest)
}

func (arch *AMD64_Arch) DoSwap(t *Node) {
	t_left  := t.children[0]
	t_right := t.children[1]

	is_float := (t_left.ty.kind == TYP_Float)

	L_memory := (t_left .kind == NP_Memory)
	R_memory := (t_right.kind == NP_Memory)

	L_isreg  := t_left .IsRegister()
	R_isreg  := t_right.IsRegister()

	// handle floating point
	if is_float {
		if L_isreg {
			arch.LoadReg(arch.workf1, t_right)
			arch.StoreReg(t_left.GetRegister(), t_right)
			arch.StoreReg(arch.workf1, t_left)

		} else if R_isreg {
			arch.LoadReg(arch.workf1, t_left)
			arch.StoreReg(t_right.GetRegister(), t_left)
			arch.StoreReg(arch.workf1, t_right)

		} else {
			arch.LoadReg(arch.workf1, t_left)
			arch.LoadReg(arch.workf2, t_right)

			arch.StoreReg(arch.workf1, t_right)
			arch.StoreReg(arch.workf2, t_left)
		}
		return
	}

	// handle case where neither operand is a register
	if !L_isreg && !R_isreg {
		if L_memory || R_memory {
			reg1 := RAX
			reg2 := R10  // cannot use R11

			arch.LoadReg(reg1, t_left)
			arch.LoadReg(reg2, t_right)

			arch.StoreReg(reg1, t_right)
			arch.StoreReg(reg2, t_left)

		} else {  // both locals
			op_left  := arch.Operand(t_left)
			op_right := arch.Operand(t_right)

			arch.LoadReg(RAX, t_left)
			arch.LoadReg(R11, t_right)

			bits := t_left.ty.bits

			OutIns("mov", "%s,%s", op_right, arch.RegNameSize(RAX, bits))
			OutIns("mov", "%s,%s", op_left,  arch.RegNameSize(R11, bits))
		}
		return
	}

	op_left  := arch.Operand(t_left)
	op_right := arch.Operand(t_right)

	OutIns("xchg", "%s,%s", op_left, op_right)
}

func (arch *AMD64_Arch) DoJump(t *Node) {
	out_label := arch.EncodeLabel(t.str)

	if t.Len() == 0 {
		OutIns("jmp", "%s", out_label)
		return
	}

	negated  := (t.flags & NF_Not) > 0
	jump_ins := "jnz"

	if negated {
		jump_ins = "jz"
	}

	t_cond := t.children[0]

	reg    := RAX
	r_name := "al"

	if t_cond.IsRegister() {
		reg    = t_cond.GetRegister()
		r_name = arch.RegNameSize(reg, 8)

	} else {
		// certain comparisons can set the CPU condition codes.
		// for these we can produce nicer assembly code.

		if t_cond.kind == NC_OpSeq {
			last := t_cond.children[t_cond.Len()-1]
			ty   := t_cond.children[t_cond.Len()-2].ty

			cc := arch.ComparisonToCC(last.str, ty)

			if cc != "" {
				if negated {
					cc = arch.NegateCC(cc)
				}
				arch.CalcOpSeq(t_cond, reg, true)
				OutIns("j" + cc, out_label)
				return
			}
		}

		arch.CalcComputation(t_cond, reg)
	}

	OutIns("test", "%s,1", r_name)
	OutIns(jump_ins, "%s", out_label)
}

func (arch *AMD64_Arch) DoReturn(t *Node) {
	arch.need_end = true

	ret_type := arch.fu.ret_type

	if t.Len() > 0 {
		// get register for result according to the ABI
		reg := RAX
		if ret_type.kind == TYP_Float {
			reg = XMM0
		}

		arch.CalcComputation(t.children[0], reg)
	}

	OutIns("jmp", ".__end")
}

func (arch *AMD64_Arch) DoPanic(t *Node) {
	reg3 := RDX
	if arch.abi == "win" {
		reg3 = R8
	}

	// load the message pointer
	arch.LoadReg(reg3, t.children[0])

	arch.EmitPanic("panic")
}

func (arch *AMD64_Arch) DoCheckNull(t *Node) {
	t_arg := t.children[0]

	reg := RAX
	if t_arg.IsRegister() {
		reg = t_arg.GetRegister()
	} else {
		arch.LoadReg(reg, t_arg)
	}

	r_name := arch.RegName(reg)

	label := arch.NewLabel(".__null")

	OutIns("test", "%s,%s", r_name, r_name)
	OutIns("jnz", "%s", label)

	arch.EmitPanic("panic_null")

	OutLine("%s:", label)
}

func (arch *AMD64_Arch) DoCheckRange(t *Node) {
	t_arg := t.children[0]

	reg := RAX
	if t_arg.IsRegister() {
		reg = t_arg.GetRegister()
	} else {
		arch.LoadReg(reg, t_arg)
	}

	r_name := arch.RegNameSize(reg, t_arg.ty.bits)

	label := arch.NewLabel(".__range")

	OutIns("cmp", "%s,%d", r_name, t.offset)
	OutIns("jb", label)

	arch.EmitPanic("panic_range")

	OutLine("%s:", label)
}

func (arch *AMD64_Arch) EmitPanic(func_name string) {
	// all three panic functions require a file number in the
	// first parameter, and line number in second parameter.

	reg1 := RDI
	reg2 := RSI

	if arch.abi == "win" {
		reg1 = RCX
		reg2 = RDX
	}

	r1_name := arch.RegName32(reg1)
	r2_name := arch.RegName32(reg2)

	// if we don't have a complete line/file combo, pass NULL as
	// the filename and zero as the line number.
	if arch.file_def == nil || arch.line_num == 0 {
		OutIns("xor", "%s,%s", r1_name, r1_name)
		OutIns("xor", "%s,%s", r2_name, r2_name)
	} else {
		out_name := arch.EncodeDefName(arch.file_def)
		OutIns("lea", "%s,[rel %s]", r1_name, out_name)
		OutIns("mov", "%s,%d", r2_name, arch.line_num)
	}

	OutIns("jmp", "$" + func_name)
}

//----------------------------------------------------------------------

func (arch *AMD64_Arch) CalcComputation(t *Node, reg Register) {
	// reg is where to store the result.
	// it can be REG_NONE when the result is not used.
	// it may be RAX or workf1, but never R11 or workf2.

	switch t.kind {
	case NC_Call:
		arch.CalcCall(t, false, reg)

	case NC_TailCall:
		arch.CalcCall(t, true, REG_NONE)

	case NC_OpSeq:
		arch.CalcOpSeq(t, reg, false)

	case NC_Matches:
		arch.CalcMatches(t, reg)

	case NC_StackVar:
		arch.CalcStackVar(t, reg)

	default:
		// node must be a normal operand
		if reg != REG_NONE {
			arch.LoadReg(reg, t)
		}
	}
}

func (arch *AMD64_Arch) CalcCall(t *Node, is_tail bool, reg Register) {
	t_func := t.children[0]
	params := t.children[1:]

	/* process the parameters */

	par_types := ParamTypesInCall(t)
	abi_regs  := arch.ParameterRegs(par_types)

	arch.HandleStackParameters(params, abi_regs)
	arch.HandleRegisterParameters(params, abi_regs)

	/* generate code to call the function */

	call_name := "r11"

	if t_func.kind == NP_GlobPtr {
		call_name = arch.EncodeDefName(t_func.def)
	} else {
		arch.LoadReg(R11, t_func)
	}

	if is_tail {
		// if we used extra registers, make sure to restore them
		arch.RestoreExtraRegs()

		OutIns("leave", "")

		// a tail-call is really a non-local jump
		OutIns("jmp", "%s", call_name)
		return
	}

	OutIns("call", "%s", call_name)

	// store the result
	if reg != REG_NONE {
		ret_type := t.ty

		if ret_type.kind == TYP_Float {
			arch.MoveReg(reg, XMM0, ret_type)
		} else {
			arch.MoveReg(reg, RAX, ret_type)
		}
	}
}

func (arch *AMD64_Arch) CallDepth(t *Node) int {
	// t must be a NC_Call node

	types := ParamTypesInCall(t)
	depth := 0

	for n, par_type := range types {
		is_float := (par_type.kind == TYP_Float)
		is_int   := ! is_float

		if arch.abi == "win" {
			depth += 1
		} else {
			if (n < 6 && is_int) || (n < 8 && is_float) {
				// not on stack
			} else {
				depth += 1
			}
		}
	}

	// for Win64 ABI, reserve space for shadow area (4 slots).
	// this area is not for THIS function, rather it is used by OTHER
	// functions we call as a place to store their first four params.

	if arch.abi == "win" && depth < 4 {
		depth = 4
	}

	return depth * 8
}

func (arch *AMD64_Arch) HandleStackParameters(params []*Node, abi_regs []Register) {
	slot := 0
	if arch.abi == "win" {
		slot = 4
	}

	for i, t_par := range params {
		if abi_regs[i] == REG_ON_STACK {
			reg := RAX

			if t_par.IsRegister() {
				reg = t_par.GetRegister()

			} else {
				if t_par.ty.kind == TYP_Float {
					reg = arch.workf1
				}
				arch.LoadReg(reg, t_par)
			}

			ins := "mov"
			if t_par.ty.kind == TYP_Float {
				ins = arch.FloatIns("movss", "movsd", t_par.ty)
			}

			slot_str := arch.EncodeStack("rsp", slot*8)
			slot += 1

			OutIns(ins, "%s,%s", slot_str, arch.RegNameSize(reg, t_par.ty.bits))
		}
	}
}

func (arch *AMD64_Arch) HandleRegisterParameters(params []*Node, abi_regs []Register) {
	// detect when loading an ABI register would overwrite the value
	// in a local variable (since they use the same register) AND that
	// local variable is needed for a later in-reg parameter.

	// WISH: if we can simply re-order the visit (esp. backwards) to avoid
	//       any overwrites, then do that instead.

	use_regs := make([]Register, len(abi_regs))
	for i, reg := range abi_regs {
		use_regs[i] = reg
	}

	swaps := make([]Register, 0)

	// need multiple passes (due to the register swapping)
	for {
		did_swap := false

		for i := range params {
			r1 := use_regs[i]
			if r1 > 0 {
				for k := i+1; k < len(params); k++ {
					r2 := use_regs[k]
					t_par := params[k]

					if r2 != REG_ON_STACK && t_par.UsesRegister(r1) {
						use_regs[i] = r2
						use_regs[k] = r1

						swaps = append(swaps, r1)
						swaps = append(swaps, r2)

						/* DEBUG
						println("OVERWRITE FOUND IN", arch.fu.name)
						println("  param", i, "overwrites local in param", k)
						println("  register", arch.RegName(r1), "<->", arch.RegName(r2))
						*/

						// need to break out of two loops (i and k)
						did_swap = true
						break
					}
				}
			}
			if did_swap {
				break
			}
		}

		if !did_swap {
			break
		}
	}

	// load the registers now
	for i, t_par := range params {
		reg := use_regs[i]

		if reg != REG_ON_STACK {
			arch.LoadReg(reg, t_par)
		}
	}

	// perform any needed swaps
	for i := len(swaps)-2; i >= 0; i -= 2 {
		r1 := swaps[i]
		r2 := swaps[i+1]

		if r1 >= XMM0 {
			ins := "movq"
			r3  := arch.workf2
			OutIns(ins, "%s,%s", arch.RegName(r3), arch.RegName(r1))
			OutIns(ins, "%s,%s", arch.RegName(r1), arch.RegName(r2))
			OutIns(ins, "%s,%s", arch.RegName(r2), arch.RegName(r3))
		} else {
			OutIns("xchg", "%s,%s", arch.RegName(r1), arch.RegName(r2))
		}
	}
}

func (arch *AMD64_Arch) CalcMatches(t *Node, reg Register) {
	if reg == REG_NONE {
		return
	}

	ty := t.children[0].ty

	cmp_reg := RAX
	cmp_ins := "cmp"

	if ty.kind == TYP_Bool {
		panic("CalcMatches with bools")
	}
	if ty.kind == TYP_Float {
		cmp_reg = arch.workf1
		cmp_ins = arch.FloatIns("comiss", "comisd", ty)
	}

	r_name := arch.RegNameSize(cmp_reg, ty.bits)

	lab_done := arch.NewLabel(".__eq_done")

	arch.LoadReg(cmp_reg, t.children[0])

	for i := 1; i < t.Len(); i++ {
		t_arg := t.children[i]

		arg2 := arch.ArgumentString(t_arg, 4|2|1)
		OutIns(cmp_ins, "%s,%s", r_name, arg2)

		if i < t.Len()-1 {
			OutIns("je", "%s", lab_done)
		}
	}

	if t.Len() > 2 {
		OutLine("%s:", lab_done)
	}

	ins := "sete"
	if (t.flags & NF_Not) > 0 {
		ins = "setne"
	}

	OutIns(ins, "%s", arch.RegName8(reg))
}

func (arch *AMD64_Arch) CalcStackVar(t *Node, reg Register) {
	if reg == REG_NONE {
		return
	}

	ofs_str := arch.EncodeStack("rbp", int(t.offset))

	OutIns("lea", "%s,%s", arch.RegName(reg), ofs_str)
}

//----------------------------------------------------------------------

func (arch *AMD64_Arch) CalcOpSeq(t *Node, reg Register, want_cc bool) {
	if reg == REG_NONE {
		// none of the operators have side effects, hence we can
		// ignore this statement.
		return
	}

	// pick register to use for first operation
	cur_reg := reg
	cur_ty  := t.children[0].ty

	if cur_reg < XMM0 && cur_ty.kind == TYP_Float {
		cur_reg = arch.workf1
	}
	if cur_reg >= XMM0 && cur_ty.kind != TYP_Float {
		cur_reg = RAX
	}

	// some instructions require the use of RAX (instead of another
	// integer register), so convert to RAX for them.

	if cur_reg > RAX && cur_reg < XMM0 {
		for _, op := range t.children[1:] {
			// integer division/remainder MUST have dividend in RAX
			if op.str == "idivt" || op.str == "udivt" || op.str == "iremt" || op.str == "uremt" {
				cur_reg = RAX
				break
			}
			// 8-bit integer multiply also requires RAX
			if op.str == "imul" && op.ty.bits == 8 {
				cur_reg = RAX
				break
			}
		}
	}

	// if one of the operands uses the same register as this one,
	// switch to using a work-horse register instead.

	for i := 0; i < t.Len(); i++ {
		t_arg := t.children[i]

		if i > 0 {
			op := t_arg
			if op.Len() == 0 {
				// unary operator, no argument here
				continue
			}
			t_arg = t_arg.children[0]
		}

		if t_arg.UsesRegister(cur_reg) {
			if cur_reg < XMM0 {
				cur_reg = RAX
			} else {
				cur_reg = arch.workf1
			}
			break
		}
	}

	// load the first argument into the main register.
	// subsequent arguments will modify that.
	arch.LoadReg(cur_reg, t.children[0])

	// apply each operator
	for i := 1; i < t.Len(); i++ {
		op := t.children[i]

		cc_only := false
		if i == t.Len()-1 {
			cc_only = want_cc
		}

		new_reg := arch.ApplyOperator(op, cur_reg, cur_ty, cc_only)
		if new_reg != REG_NONE {
			cur_reg = new_reg
		}

		cur_ty = op.ty
	}

	if !want_cc {
		// store result
		arch.MoveReg(reg, cur_reg, t.ty)
	}
}

func (arch *AMD64_Arch) ApplyOperator(op *Node, reg Register, ty *Type, cc_only bool) Register {
	new_reg := REG_NONE

	name := op.str

	arg1 := arch.RegNameSize(reg, ty.bits)

	var t_arg *Node
	if op.Len() > 0 {
		t_arg = op.children[0]
	}

	/* comparisons (via CC) */

	switch name {
	case "bnot",
		"ieq?", "ine?", "ilt?", "ile?", "igt?", "ige?",
		"feq?", "fne?", "flt?", "fle?", "fgt?", "fge?",
		"ueq?", "une?", "ult?", "ule?", "ugt?", "uge?",
		"peq?", "pne?", "plt?", "ple?", "pgt?", "pge?",
		"ipos?", "ineg?", "fpos?", "fneg?",
		"izero?", "isome?", "fzero?", "fsome?",
		"pnull?", "pref?", "finf?", "fnan?":

		arch.Comparison(op, ty, arg1)

		if !cc_only {
			cc := arch.ComparisonToCC(name, ty)
			if cc == "" {
				panic("no CC for comparison")
			}

			out_name := arch.RegNameSize(RAX, 8)
			OutIns("set" + cc, "%s", out_name)
			new_reg = RAX
		}
		return new_reg
	}

	if cc_only {
		panic("cc_only with " + name)
	}

	switch name {
	/* conversions */

	case "iext":
		return arch.Extend_signed(reg, ty, op.ty)

	case "uext":
		return arch.Extend_unsigned(reg, ty, op.ty)

	case "fext":
		return arch.Extend_float(reg, ty, op.ty)

	case "itof":
		return arch.Convert_signed_float(reg, ty, op.ty)

	case "utof":
		return arch.Convert_unsigned_float(reg, ty, op.ty)

	case "ftoi":
		return arch.Convert_float_signed(reg, ty, op.ty)

	case "ftou":
		return arch.Convert_float_unsigned(reg, ty, op.ty)

	case "btoi":
		return arch.Convert_bool_int(reg, op.ty)

	case "raw-cast":
		return arch.RawCast(reg, ty, op.ty)

	/* integer operations */

	case "ineg":
		OutIns("neg", "%s", arg1)

	case "iflip":
		OutIns("not", "%s", arg1)

	case "iabs":
		arg2 := arch.RegNameSize(R11, ty.bits)

		OutIns("mov",  "%s,%s", arg2, arg1)
		OutIns("neg",  "%s",    arg2)
		OutIns("test", "%s,%s", arg1, arg1)

		// CMOV does not support 8-bit registers
		arg1 = arch.RegName(reg)
		arg2 = arch.RegName(R11)

		OutIns("cmovs", "%s,%s", arg1, arg2)

	case "iadd", "padd":
		arg2 := arch.ArgumentString(t_arg, 4|2|1)
		OutIns("add", "%s,%s", arg1, arg2)

	case "isub", "psub", "pdiff":
		arg2 := arch.ArgumentString(t_arg, 4|2|1)
		OutIns("sub", "%s,%s", arg1, arg2)

	case "iand", "band", "bmin":
		arg2 := arch.ArgumentString(t_arg, 4|2|1)
		OutIns("and", "%s,%s", arg1, arg2)

	case "ior", "bor", "bmax":
		arg2 := arch.ArgumentString(t_arg, 4|2|1)
		OutIns("or", "%s,%s", arg1, arg2)

	case "ixor", "bxor":
		arg2 := arch.ArgumentString(t_arg, 4|2|1)
		OutIns("xor", "%s,%s", arg1, arg2)

	case "imul":
		// NOTE: we use the IMUL instruction for both signed and unsigned
		// integers, possible due to two's complement notation and that we
		// don't need to detect overflow.  it is also what GCC does.

		if t_arg.ty.bits == 8 {
			// 8-bit integer multiply does not allow an immediate
			arg2 := arch.ArgumentString(t_arg, 4|1)
			OutIns("imul", "%s", arg2)
		} else {
			// the second arg here may be an immediate value, which strictly
			// speaking should use the 3-arg form, but NASM allows it.
			arg2 := arch.ArgumentString(t_arg, 4|2|1)
			OutIns("imul", "%s,%s", arg1, arg2)
		}

	case "idivt":
		arch.BU_int_divrem(t_arg, t_arg.ty, false, false)

	case "udivt":
		arch.BU_int_divrem(t_arg, t_arg.ty, false, true)

	case "iremt":
		arch.BU_int_divrem(t_arg, t_arg.ty, true, false)

	case "uremt":
		arch.BU_int_divrem(t_arg, t_arg.ty, true, true)

	case "ishl", "ushl":
		arch.BU_int_shift(t_arg, reg, ty, "shl")

	case "ishr":
		arch.BU_int_shift(t_arg, reg, ty, "sar")

	case "ushr":
		arch.BU_int_shift(t_arg, reg, ty, "shr")

	case "imin":
		// this needs a register (since it uses CMOV)
		reg2 := t_arg.GetRegister()
		if reg2 == REG_NONE { reg2 = R11 }
		arch.LoadReg(reg2, t_arg)
		arch.BU_int_minmax(ty, reg, reg2, "cmovg")

	case "umin", "pmin":
		// this needs a register (since it uses CMOV)
		reg2 := t_arg.GetRegister()
		if reg2 == REG_NONE { reg2 = R11 }
		arch.LoadReg(reg2, t_arg)
		arch.BU_int_minmax(ty, reg, reg2, "cmova")

	case "imax":
		// this needs a register (since it uses CMOV)
		reg2 := t_arg.GetRegister()
		if reg2 == REG_NONE { reg2 = R11 }
		arch.LoadReg(reg2, t_arg)
		arch.BU_int_minmax(ty, reg, reg2, "cmovl")

	case "umax", "pmax":
		// this needs a register (since it uses CMOV)
		reg2 := t_arg.GetRegister()
		if reg2 == REG_NONE { reg2 = R11 }
		arch.LoadReg(reg2, t_arg)
		arch.BU_int_minmax(ty, reg, reg2, "cmovb")

	case "ilittle":
		// nothing needed

	case "iswap", "ibig":
		switch ty.bits {
		case 8:
			// nothing needed

		case 16:
			if reg != RAX {
				OutIns("mov", "ax,%s", arg1)
			}
			OutIns("xchg", "al,ah")
			if reg != RAX {
				OutIns("mov", "%s,ax", arg1)
			}

		default:
			OutIns("bswap", "%s", arg1)
		}

	/* float operations */

	case "fneg":
		// produce a mask which flips the top bit
		mask := arch.RegNameSize(RAX, ty.bits)
		work := arch.RegName(arch.workf2)

		OutIns("xor", "%s,%s", mask, mask)
		OutIns("stc", "")
		OutIns("rcr", "%s,1", mask)

		ins := arch.FloatIns("movd", "movq", ty)
		OutIns(ins, "%s,%s", work, mask)

		ins = arch.FloatIns("xorps", "xorpd", ty)
		OutIns(ins, "%s,%s", arg1, work)

	case "fabs":
		// produce a mask which clears the top bit
		mask := arch.RegNameSize(RAX, ty.bits)
		work := arch.RegName(arch.workf2)

		OutIns("xor", "%s,%s", mask, mask)
		OutIns("dec", "%s", mask)
		OutIns("shr", "%s,1", mask)

		ins := arch.FloatIns("movd", "movq", ty)
		OutIns(ins, "%s,%s", work, mask)

		ins = arch.FloatIns("andps", "andpd", ty)
		OutIns(ins, "%s,%s", arg1, work)

	case "fsqrt":
		ins := arch.FloatIns("sqrtss", "sqrtsd", ty)
		OutIns(ins, "%s,%s", arg1, arg1)

	case "fadd":
		arg2 := arch.ArgumentString(t_arg, 1)
		ins  := arch.FloatIns("addss", "addsd", t_arg.ty)
		OutIns(ins, "%s,%s", arg1, arg2)

	case "fsub":
		arg2 := arch.ArgumentString(t_arg, 1)
		ins  := arch.FloatIns("subss", "subsd", t_arg.ty)
		OutIns(ins, "%s,%s", arg1, arg2)

	case "fmul":
		arg2 := arch.ArgumentString(t_arg, 1)
		ins  := arch.FloatIns("mulss", "mulsd", t_arg.ty)
		OutIns(ins, "%s,%s", arg1, arg2)

	case "fdiv":
		arg2 := arch.ArgumentString(t_arg, 1)
		ins  := arch.FloatIns("divss", "divsd", t_arg.ty)
		OutIns(ins, "%s,%s", arg1, arg2)

	case "fremt":
		// if the args are N and D, we need to compute this:
		//    N - trunc(N / D) * D

		suffix := "sd"
		if ty.bits == 32 {
			suffix = "ss"
		}

		// use XMM3 register to hold the division
		div  := arch.RegName(XMM3)
		mode := 0x3  // truncate
		arg2 := arch.ArgumentString(t_arg, 1)

		OutIns("movq",           "%s,%s", div, arg1)
		OutIns("div"   + suffix, "%s,%s", div, arg2)
		OutIns("round" + suffix, "%s,%s,0x%d", div, div, mode)
		OutIns("mul"   + suffix, "%s,%s", div, arg2)
		OutIns("sub"   + suffix, "%s,%s", arg1, div)

	case "fmax":
		arg2 := arch.ArgumentString(t_arg, 1)
		ins  := arch.FloatIns("maxss", "maxsd", t_arg.ty)
		OutIns(ins, "%s,%s", arg1, arg2)

	case "fmin":
		arg2 := arch.ArgumentString(t_arg, 1)
		ins  := arch.FloatIns("minss", "minsd", t_arg.ty)
		OutIns(ins, "%s,%s", arg1, arg2)

	case "fround", "ftrunc", "ffloor", "fceil":
		var mode int
		switch name {
		case "fround": mode = 0x0
		case "ffloor": mode = 0x1
		case "fceil":  mode = 0x2
		case "ftrunc": mode = 0x3
		}

		ins := arch.FloatIns("roundss", "roundsd", ty)
		OutIns(ins, "%s,%s,0x%x", arg1, arg1, mode)

	/* bool comparisons */

	case "beq?":
		arg2 := arch.ArgumentString(t_arg, 4|2|1)
		OutIns("xor", "%s,%s", arg1, arg2)
		OutIns("xor", "%s,1",  arg1)

	case "bne?":
		arg2 := arch.ArgumentString(t_arg, 4|2|1)
		OutIns("xor", "%s,%s", arg1, arg2)

	case "blt?":
		// `a < b` is equivalent to `!a & b`
		arg2 := arch.ArgumentString(t_arg, 4|2|1)
		OutIns("xor", "%s,1",  arg1)
		OutIns("and", "%s,%s", arg1, arg2)

	case "bgt?":
		// `a > b` is equivalent to `a & !b` as well as `!(!a | b)`
		arg2 := arch.ArgumentString(t_arg, 4|2|1)
		if arg2 == "r11b" {
			OutIns("xor", "%s,1",  arg2)
			OutIns("and", "%s,%s", arg1, arg2)
		} else {
			OutIns("xor", "%s,1",  arg1)
			OutIns("or",  "%s,%s", arg1, arg2)
			OutIns("xor", "%s,1",  arg1)
		}

	case "ble?":
		// `a <= b` is equivalent to `!a | b`
		arg2 := arch.ArgumentString(t_arg, 4|2|1)
		OutIns("xor", "%s,1",  arg1)
		OutIns("or",  "%s,%s", arg1, arg2)

	case "bge?":
		// `a >= b` is equivalent to `a | !b` as well as `!(!a & b)`
		arg2 := arch.ArgumentString(t_arg, 4|2|1)
		if arg2 == "r11b" {
			OutIns("xor", "%s,1",  arg2)
			OutIns("or",  "%s,%s", arg1, arg2)
		} else {
			OutIns("xor", "%s,1",  arg1)
			OutIns("and", "%s,%s", arg1, arg2)
			OutIns("xor", "%s,1",  arg1)
		}

	default:
		panic("unknown operation: " + name)
	}

	return new_reg
}

func (arch *AMD64_Arch) Comparison(op *Node, ty *Type, arg1 string) {
	name := op.str

	switch name {
	case "bnot":
		OutIns("test", "%s,1", arg1)

	case "ieq?", "ine?", "ilt?", "ile?", "igt?", "ige?",
	     "ueq?", "une?", "ult?", "ule?", "ugt?", "uge?",
	     "peq?", "pne?", "plt?", "ple?", "pgt?", "pge?":
		t_arg := op.children[0]
		arg2  := arch.ArgumentString(t_arg, 4|2|1)
		OutIns("cmp", "%s,%s", arg1, arg2)

	case "feq?", "fne?", "flt?", "fle?", "fgt?", "fge?":
		t_arg := op.children[0]
		arg2  := arch.ArgumentString(t_arg, 1)
		ins   := arch.FloatIns("comiss", "comisd", ty)
		OutIns(ins, "%s,%s", arg1, arg2)

	case "izero?", "isome?", "ineg?", "ipos?", "pnull?", "pref?":
		OutIns("test", "%s,%s", arg1, arg1)

	case "fzero?", "fsome?", "fneg?", "fpos?":
		if ty.bits == 32 {
			OutIns("movd", "eax,%s", arg1)
			OutIns("shl",  "eax,1")
		} else {
			OutIns("movq", "rax,%s", arg1)
			OutIns("shl",  "rax,1")
		}

		if name == "fneg?" {
			OutIns("cmc", "")
		}

	case "finf?":
		// require exponent is all 1s, fraction is all 0s
		if ty.bits == 32 {
			OutIns("movd", "eax,%s", arg1)
			OutIns("xor",  "eax,0x7F800000")
			OutIns("shl",  "eax,1")
		} else {
			OutIns("movq", "rax,%s", arg1)
		}

	case "fnan?":
		// require exponent is all 1s, fraction is non-zero
		if ty.bits == 32 {
			OutIns("movd", "eax,%s", arg1)
			OutIns("shl",  "eax,1")
			OutIns("cmp",  "eax,0xFF000000")
		} else {
			OutIns("movq", "rax,%s", arg1)
			OutIns("mov",  "r11d,0xFFE00000")
			OutIns("shl",  "r11,32")
			OutIns("shl",  "rax,1")
			OutIns("cmp",  "rax,r11")
		}

	default:
		panic("unknown comparison: " + name)
	}
}

func (arch *AMD64_Arch) Extend_signed(reg Register, src_type, dest_type *Type) Register {
	if dest_type.bits > src_type.bits {
		switch src_type.bits {
		case 8:  OutIns("movsx", "%s,%s", arch.RegName(reg), arch.RegName8(reg))
		case 16: OutIns("movsx", "%s,%s", arch.RegName(reg), arch.RegName16(reg))
		case 32: OutIns("movsxd","%s,%s", arch.RegName(reg), arch.RegName32(reg))
		}
	}
	return REG_NONE
}

func (arch *AMD64_Arch) Extend_unsigned(reg Register, src_type, dest_type *Type) Register {
	// Note that the AMD64 architecture automatically zero extends
	// to 64 bits when moving something to a 32-bit register.
	// [ hence no `movzx` for the 32-bit case below ]

	if dest_type.bits > src_type.bits {
		switch src_type.bits {
		case 8:  OutIns("movzx", "%s,%s", arch.RegName(reg),   arch.RegName8(reg))
		case 16: OutIns("movzx", "%s,%s", arch.RegName(reg),   arch.RegName16(reg))
		case 32: OutIns("mov",   "%s,%s", arch.RegName32(reg), arch.RegName32(reg))
		}
	}
	return REG_NONE
}

func (arch *AMD64_Arch) Convert_bool_int(reg Register, dest_type *Type) Register {
	if dest_type.bits < 64 {
		OutIns("and", "%s,1", arch.RegNameSize(reg, dest_type.bits))
	} else {
		OutIns("mov", "r11d,1")
		OutIns("and", "%s,r11", arch.RegName(reg))
	}
	return REG_NONE
}

func (arch *AMD64_Arch) Extend_float(reg Register, src_type, dest_type *Type) Register {
	if src_type.bits != dest_type.bits {
		r_name := arch.RegName(reg)
		ins    := arch.FloatIns("cvtsd2ss", "cvtss2sd", dest_type)
		OutIns(ins, "%s,%s", r_name, r_name)
	}
	return REG_NONE
}

func (arch *AMD64_Arch) Convert_signed_float(reg Register, src_type, dest_type *Type) Register {
	src_name := arch.RegName(reg)

	if src_type.bits < 64 {
		src_name = arch.RegName32(reg)

		// need an integer at least 32-bits wide, extend if needed
		switch src_type.bits {
		case 8:  OutIns("movsx", "%s,%s", src_name, arch.RegName8(reg))
		case 16: OutIns("movsx", "%s,%s", src_name, arch.RegName16(reg))
		}
	}

	dest_name := arch.RegName(arch.workf1)

	ins := "cvtsi2sd"
	if dest_type.bits < 64 {
		ins = "cvtsi2ss"
	}

	OutIns(ins, "%s,%s", dest_name, src_name)

	return arch.workf1
}

func (arch *AMD64_Arch) Convert_unsigned_float(reg Register, src_type, dest_type *Type) Register {
	if src_type.bits < 32 {
		arch.Extend_unsigned(reg, src_type, i32_type)
		return arch.Convert_signed_float(reg, i32_type, dest_type)
	}

	if src_type.bits < 64 {
		arch.Extend_unsigned(reg, src_type, i64_type)
		return arch.Convert_signed_float(reg, i64_type, dest_type)
	}

	// u64 is problematic since there is no AMD64 instruction to convert
	// from u64 to a float.  hence we need to do a little dance....

	// TODO this code MODIFIES the integer register, review if that is okay
	//      or whether we should force into RAX.

	src_name  := arch.RegName(reg)
	dest_name := arch.RegName(arch.workf1)

	ins := "cvtsi2sd"
	if dest_type.bits < 64 {
		ins = "cvtsi2ss"
	}

	big_label  := arch.NewLabel(".__fconv_big")
	done_label := arch.NewLabel(".__fconv_done")

	OutIns("test", "%s,%s", src_name, src_name)
	OutIns("js", "%s", big_label)

	// top bit is clear
	OutIns(ins, "%s,%s", dest_name, src_name)
	OutIns("jmp", "%s", done_label)

	OutLine("%s:", big_label)

	// top bit is set: divide by two, convert, multiply by two
	OutIns("shr", "%s,1", src_name)
	OutIns(ins, "%s,%s", dest_name, src_name)

	ins = arch.FloatIns("addss", "addsd", dest_type)
	OutIns(ins, "%s,%s", dest_name, dest_name)

	OutLine("%s:", done_label)

	return arch.workf1
}

func (arch *AMD64_Arch) Convert_float_signed(reg Register, src_type, dest_type *Type) Register {
	src_name  := arch.RegName(reg)
	dest_name := arch.RegName(RAX)

	if dest_type.bits < 64 {
		dest_name = arch.RegName32(RAX)
	}

	ins := "cvtsd2si"
	if src_type.bits < 64 {
		ins = "cvtss2si"
	}

	OutIns(ins, "%s,%s", dest_name, src_name)

	// WISH: check if value is too large for 8/16 bit destination,
	//       and if so produce 0x80/0x8000.

	return RAX
}

func (arch *AMD64_Arch) Convert_float_unsigned(reg Register, src_type, dest_type *Type) Register {
	// FIXME ftou operation
	return arch.Convert_float_signed(reg, src_type, dest_type)
}

func (arch *AMD64_Arch) RawCast(reg Register, src_type, dest_type *Type) Register {
	src_float  := (src_type.kind  == TYP_Float)
	dest_float := (dest_type.kind == TYP_Float)

	// nothing special is needed unless we are casting between a float
	// and a non-float....

	if src_float == dest_float {
		// okay
		return REG_NONE

	} else if src_float {
		// float --> integer

		ins       := arch.FloatIns("movd", "movq", src_type)
		src_name  := arch.RegName(reg)
		dest_name := arch.RegNameSize(RAX, dest_type.bits)

		OutIns(ins, "%s,%s", dest_name, src_name)

		return RAX

	} else {
		// integer --> float

		ins       := arch.FloatIns("movd", "movq", dest_type)
		src_name  := arch.RegNameSize(reg, src_type.bits)
		dest_name := arch.RegName(arch.workf1)

		OutIns(ins, "%s,%s", dest_name, src_name)

		return arch.workf1
	}
}

func (arch *AMD64_Arch) ComparisonToCC(name string, ty *Type) string {
	// NOTE: floating point compares are like unsigned integer compares,
	// they distinguish less/greater via the carry flag (CF).

	switch name {
	case "bnot": return "z"

	case "ieq?", "ueq?", "peq?", "feq?": return "e"
	case "ine?", "une?", "pne?", "fne?": return "ne"

	case "ilt?":  return "l"
	case "ile?":  return "le"
	case "igt?":  return "g"
	case "ige?":  return "ge"

	case "ult?", "plt?", "flt?":  return "b"
	case "ule?", "ple?", "fle?":  return "be"
	case "ugt?", "pgt?", "fgt?":  return "a"
	case "uge?", "pge?", "fge?":  return "ae"

	case "ipos?": return "g"
	case "ineg?": return "s"

	case "izero?", "fzero?", "pnull?": return "z"
	case "isome?", "fsome?", "pref?":  return "nz"

	case "fneg?": return "a"
	case "fpos?": return "a"
	case "finf?": return "z"
	case "fnan?": return "a"
	}

	return ""
}

func (arch *AMD64_Arch) NegateCC(cc string) string {
	if cc[0] == 'n' {
		return cc[1:]
	}
	return "n" + cc
}

func (arch *AMD64_Arch) BU_int_shift(t_shift *Node, reg Register, ty *Type, ins string) {
	// shift operations must use the CL register or a literal value.
	// if the output register is RCX, things get a bit tricky....

	use_reg := reg
	if reg == RCX && t_shift.kind != NL_Integer {
		use_reg = RAX
		OutIns("mov", "rax,rcx")
	}

	arg_str := arch.RegNameSize(use_reg, ty.bits)

	shift_str := "cl"

	if t_shift.kind == NL_Integer {
		// parsing code has checked for a valid shift count
		shift_str = t_shift.str

	} else {
		arch.LoadReg(RCX, t_shift)

		// limit to width of type to prevent UB (Undefined Behavior)
		mask_str := strconv.Itoa(ty.bits - 1)
		OutIns("and", "cl,%s", mask_str)
	}

	OutIns(ins, "%s,%s", arg_str, shift_str)

	if reg == RCX && t_shift.kind != NL_Integer {
		OutIns("mov", "rcx,rax")
	}
}

func (arch *AMD64_Arch) BU_int_divrem(t_arg2 *Node, ty *Type, is_rem bool, unsigned bool) {
	// these do not allow an immediate, but do need a memory size prefix
	// the 1 flag forces value into R11 (so we don't overwrite RAX).
	arg2 := arch.ArgumentString(t_arg2, 8|4|1)

	// the DIV and IDIV instructions are tricky.
	// the dividend must be in AX, DX:AX, EDX:EAX or RDX:RAX combos.
	// result stores the quotient in RAX, remainder in RDX.

	if unsigned {
		// clear the upper portion
		if ty.bits == 8 {
			OutIns("xor", "ah,ah")
		} else {
			OutIns("xor", "rdx,rdx")
		}
		OutIns("div", "%s", arg2)

	} else {
		// set upper to all 0s or all 1s, depending on sign of divisor
		switch ty.bits {
		case 8:  OutIns("cbw", "")
		case 16: OutIns("cwd", "")
		case 32: OutIns("cdq", "")
		default: OutIns("cqo", "")
		}
		OutIns("idiv", "%s", arg2)
	}

	if is_rem {
		if ty.bits == 8 {
			OutIns("mov", "al,ah")
		} else {
			OutIns("mov", "rax,rdx")
		}
	}
}

func (arch *AMD64_Arch) BU_int_minmax(ty *Type, reg1, reg2 Register, ins string) {
	arg1 := arch.RegNameSize(reg1, ty.bits)
	arg2 := arch.RegNameSize(reg2, ty.bits)

	OutIns("cmp", "%s,%s", arg1, arg2)

	// CMOV does not support 8-bit registers
	arg1 = arch.RegName(reg1)
	arg2 = arch.RegName(reg2)

	OutIns(ins, "%s,%s", arg1, arg2)
}

//----------------------------------------------------------------------

func (arch *AMD64_Arch) ArgumentString(op *Node, flags int) string {
	var reg Register

	if op.IsRegister() {
		reg = op.GetRegister()
		return arch.RegNameSize(reg, op.ty.bits)
	}

	// float instructions usually require registers
	if op.ty.kind == TYP_Float {
		reg = arch.workf1
		if (flags & 1) != 0 {
			reg = arch.workf2
		}
		arch.LoadReg(reg, op)
		return arch.RegName(reg)
	}

	// allow an immediate?
	if (flags & 2) != 0 {
		switch op.kind {
		case NL_Integer, NL_Char:
			// 64-bits is ok when value fits into 32
			if op.ty.bits > 32 && IntegerFitting(op, false) > 32 {
				// too large
			} else {
				prefix := ""
				if (flags & 8) != 0 {
					prefix = arch.SizePrefix(op.ty.bits)
				}
				return prefix + arch.Operand(op)
			}
		}
	}

	// allow a memory access?
	if (flags & 4) != 0 {
		if op.kind == NP_Local || op.kind == NP_Memory {
			prefix := ""
			if (flags & 8) != 0 {
				prefix = arch.SizePrefix(op.ty.bits)
			}
			return prefix + arch.Operand(op)
		}
	}

	reg = RAX
	if (flags & 1) != 0 {
		reg = R11
	}

	arch.LoadReg(reg, op)
	return arch.RegNameSize(reg, op.ty.bits)
}

func (arch *AMD64_Arch) Operand(op *Node) string {
	switch op.kind {
	case NL_Integer, NL_Char, NL_Float, NL_FltSpec, NL_String:
		return arch.DataValue(op)

	case NP_Local:
		local := op.local
		if local.reg == REG_ON_STACK {
			return arch.EncodeStack("rbp", local.offset)
		} else if local.ty.kind == TYP_Float {
			return arch.RegName(local.reg)
		} else {
			return arch.RegNameSize(local.reg, local.ty.bits)
		}

	case NP_Memory:
		base := ""
		base_op := op.children[0]

		if base_op.kind == NP_GlobPtr {
			base = "rel " + arch.EncodeDefName(base_op.def)
		} else {
			base = arch.Operand(base_op)

			// handle a local on the stack, move ptr into R11
			if base_op.kind == NP_Local {
				local := base_op.local

				if local.reg == REG_ON_STACK {
					base = arch.RegName(R11)
					slot_str := arch.EncodeStack("rbp", local.offset)
					OutIns("mov", "%s,%s", base, slot_str)
				}
			}
		}

		if op.offset > 0 {
			return fmt.Sprintf("[%s+%d]", base, op.offset)
		} else {
			return fmt.Sprintf("[%s]", base)
		}

	default:
		panic("unknown operand node")
	}
}

func (arch *AMD64_Arch) LiteralToReg(reg Register, lit *Node) {
	if lit.ty == nil {
		panic("literal node did not get a type")
	}
	ty := lit.ty

	r_name := arch.RegName(reg)

	if lit.kind == NL_Float || lit.kind == NL_FltSpec {
		if lit.str == "0.0" || lit.str == "0.0e0" || lit.str == "0x0p0" {
			OutIns("pxor", "%s,%s", r_name, r_name)
			return
		}

		val_str := arch.DataValue(lit)

		glob := ""
		ins  := ""

		switch ty.bits {
		case 32:
			glob = arch.AddFloat32(val_str)
			ins  = "movss"
		default:
			glob = arch.AddFloat64(val_str)
			ins  = "movsd"
		}

		OutIns(ins, "%s,%s", r_name, glob)
		return
	}

	if lit.kind == NL_String {
		glob := arch.DataValue(lit)
		OutIns("lea", "%s,[rel %s]", r_name, glob)
		return
	}

	switch lit.kind {
	case NL_Integer, NL_Char:
		// ok
	default:
		panic("unsupported literal kind")
	}

	if lit.str == "0" || lit.str == "0x0" {
		OutIns("xor", "%s,%s", r_name, r_name)
		return
	}

	if ty.bits < 64 {
		r_name := arch.RegNameSize(reg, ty.bits)
		OutIns("mov", "%s,%s", r_name, lit.str)

	} else {
		if lit.str[0] == '-' && IntegerFitting(lit, false) <= 32 {
			// the CPU sign extends here
			OutIns("mov", "%s,dword %s", r_name, lit.str)

		} else if lit.str[0] != '-' && IntegerFitting(lit, true) <= 32 {
			// the CPU zero extends here
			OutIns("mov", "%s,%s", arch.RegName32(reg), lit.str)

		} else {
			// use a 64-bit immediate move (called `movabs` in gas)
			OutIns("mov", "%s,%s", r_name, lit.str)
		}
	}
}

func (arch *AMD64_Arch) MoveReg(dest, src Register, ty *Type) {
	if dest != src {
		ins := "mov"

		if ty.kind == TYP_Float {
			ins = arch.FloatIns("movss", "movsd", ty)
		}

		OutIns(ins, "%s,%s", arch.RegName(dest), arch.RegName(src))
	}
}

func (arch *AMD64_Arch) LoadReg(reg Register, op *Node) {
	if op.ty == nil {
		panic("LoadReg with no type")
	}

	// nothing needed if operand is the same register
	if op.IsRegister() {
		local := op.local
		if local.reg == reg {
			return
		}
	}

	if op.IsLiteral() {
		arch.LiteralToReg(reg, op)
		return
	}

	ty := op.ty

	r_name := arch.RegName(reg)
	ins    := "mov"

	if op.kind == NP_GlobPtr {
		out_name := arch.EncodeDefName(op.def)
		OutIns("lea", "%s,[rel %s]", r_name, out_name)
		return
	}

	if ty.kind == TYP_Float {
		ins = arch.FloatIns("movss", "movsd", ty)
	} else {
		r_name = arch.RegNameSize(reg, ty.bits)
	}

	OutIns(ins, "%s,%s", r_name, arch.Operand(op))
}

func (arch *AMD64_Arch) StoreReg(reg Register, op *Node) {
	if op.ty == nil {
		panic("StoreReg with no type")
	}

	// this is not possible due to use of R11 in Operand()
	if reg == R11 {
		panic("StoreReg with R11")
	}

	// nothing needed if destination is the same register
	if op.IsRegister() {
		local := op.local
		if local.reg == reg {
			return
		}
	}

	ty := op.ty

	r_name := arch.RegName(reg)
	ins    := "mov"

	if ty.kind == TYP_Float {
		ins = arch.FloatIns("movss", "movsd", ty)
	} else {
		r_name = arch.RegNameSize(reg, ty.bits)
	}

	OutIns(ins, "%s,%s", arch.Operand(op), r_name)
}

func (arch *AMD64_Arch) StoresToDerefLocal(t *Node, local *LocalVar) bool {
	if t.kind != NH_Move {
		return false
	}

	dest := t.children[0]
	if dest.kind != NP_Memory {
		return false
	}

	base := dest.children[0]
	if base.kind != NP_Local {
		return false
	}

	return base.local == local
}

func (arch *AMD64_Arch) SameLocalOrMem(t1, t2 *Node) bool {
	return arch.SameLocals(t1, t2) || arch.SameMemory(t1, t2)
}

func (arch *AMD64_Arch) SameLocals(t1, t2 *Node) bool {
	if t1.kind == NP_Local && t2.kind == NP_Local {
		return t1.local == t2.local
	}
	return false
}

func (arch *AMD64_Arch) SameMemory(t1, t2 *Node) bool {
	if t1.kind == NP_Memory && t2.kind == NP_Memory {
		if t1.offset != t2.offset {
			return false
		}
		t1 = t1.children[0]
		t2 = t2.children[0]

		if t1.kind == NP_GlobPtr && t2.kind == NP_GlobPtr {
			return t1.str == t2.str
		}
		return arch.SameLocals(t1, t2)
	}

	return false
}

func (arch *AMD64_Arch) FloatIns(ins32, ins64 string, ty *Type) string {
	if ty.bits == 32 {
		return ins32
	}
	return ins64
}

func (arch *AMD64_Arch) SizePrefix(bits int) string {
	switch bits {
	case 8:  return "byte "
	case 16: return "word "
	case 32: return "dword "
	default: return "qword "
	}
}

func (arch *AMD64_Arch) NewLabel(prefix string) string {
	arch.new_labels += 1
	return prefix + strconv.Itoa(arch.new_labels)
}

//----------------------------------------------------------------------

func (arch *AMD64_Arch) SortLocals() {
	arch.ordered_locals = make([]*LocalVar, 0)

	for _, local := range arch.fu.locals {
		arch.ordered_locals = append(arch.ordered_locals, local)
	}

	sort.Slice(arch.ordered_locals, func(i, k int) bool {
		L1 := arch.ordered_locals[i]
		L2 := arch.ordered_locals[k]

		return L1.OrdLess(L2)
	})
}
